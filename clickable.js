var buttonList = document.getElementsByClassName("boutonDeroulant");

// Adding the onClick listener to all the "bouttonDeroulant"

for(let i=0; i < buttonList.length; i++)
{
  buttonList[i].onclick = function(e) {
    (document.getElementsByClassName("menuContenu"))[i].classList.toggle("show");
  }
}

window.onclick = function(event) {
  if (!event.target.matches('.boutonDeroulant') && !event.target.matches('.dontClose')) {
    let dropdowns = document.getElementsByClassName("menuContenu");
    for (let i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}


function launchAlert() {
  alert("Vous avez cliqué n'est ce pas ?");
}
